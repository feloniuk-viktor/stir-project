$(document).ready(function () {
    let position = 0;
    const slidesToShow = 4; //Скільки показати
    const slidesToScroll = 2; //Скільки скролити
    const container = $('.slider-container')
    const track = $('.slider-track')
    const item = $('.slider-item')
    const itemCount = item.length
    const btnPrev = $('.btn-prev')
    const btnNext = $('.btn-next')
    const itemWith = container.width() / slidesToShow
    const movePosition = slidesToScroll * itemWith

    item.each(function (index, item) {
        $(item).css({
            minWidth: itemWith,
        });
    });
    // console.log(container.width()," ",slidesToShow)

    btnPrev.click(function () {
        const itemLeft = Math.abs(position) / itemWith
        position += itemLeft >= slidesToScroll ? movePosition : itemLeft * itemWith
        setPosition()
        checkButtons()
    })

    btnNext.click(function () {
        const itemLeft = itemCount - (Math.abs(position) + slidesToShow * itemWith) / itemWith
        position -= itemLeft >= slidesToScroll ? movePosition : itemLeft * itemWith
        setPosition()
        checkButtons()

        // console.log(btnNext.disabled)
    })


    const setPosition = () => {
        track.css({
            transform: `translateX(${position}px)`
        })
    }

    const checkButtons = () => {
        btnPrev.prop('disabled', position === 0)
        btnNext.prop('disabled', position <= -(itemCount - slidesToShow) * itemWith)

    }

    checkButtons()
})